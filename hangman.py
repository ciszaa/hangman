import random

gallows = [
        """        """,
        """
         
         
         
         
         
         
        /\\""",
        """
        |
        |
        |
        |
        |
        |
        /\\""",
        """
        |----------
        |
        |
        |
        |
        |
        /\\""",
        """
        |----------
        |           |
        |
        |
        |
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |
        |
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |           |
        |
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |          /|
        |
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |          /|\\
        |
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |          /|\\
        |           \\
        |
        /\\""",
        """
        |----------
        |           |
        |           0
        |          /|\\
        |           /\\
        |
        /\\""",
    ]

words = ["Elephant", "Computer", "Sunshine", "Backpack", "Chocolate", "Rainbow", "Guitar", "Butterfly", "Umbrella", "Adventure"]

word = random.choice(words).upper()

num_of_errors = 0
max_errors = 10 
guessed_letters = []
result = ["_"] * len(word)

print("Welcome to the Hangman! Let's start. ")
print(f"Your word to guess: {" ".join(result)}\n")

while num_of_errors <= max_errors:
    user_guess = input("\nGuess a letter: ").upper() 

    if user_guess in guessed_letters:
        print("You have already typed this letter.\n")
        print(f"Guessed letters: {guessed_letters}")
        continue

    if user_guess in word:
        print("That's a good letter!")
        for i in range(len(word)):
            if word[i] == user_guess:
                result[i] = user_guess
        print(" ".join(result))

    if "_" not in result:
        print("\nCongratulations! You have won the game.")
        break

    if user_guess not in word:
        num_of_errors = num_of_errors + 1
        guessed_letters.append(user_guess)
        if num_of_errors < 10:
            print("That's a wrong letter!")
            print(gallows[num_of_errors] + "\n")
            print(f"Guessed letters: {guessed_letters}")
            print(" ".join(result))
        else:
            print(gallows[num_of_errors])
            print(f"Game over! Correct word was: {word}")
            break

